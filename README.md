# Nodejs

**Install dependencies**
```
npm i
```

**Start development**
```
npm run dev
```

*By default the dev server starts at port 8080, if it is in use you can set another from param*

```
npm run dev -- --port 3000
```

**Build production**

```
npm run build
```

**Static code analysis (linting)**
```
npm run lint
```

**Single run unit tests (in PhantomJS)**

```
npm test
```

**Run unit tests in Chrome and watch (rerun on code change)**

```
npm run watch:test
```

*You can change browser from param*

```
npm run watch:test -- --browsers Firefox
```

```
npm run watch:test -- --browsers PhantomJS
```

```
npm run watch:test -- --browsers Chrome,Firefox
```

## ISC License (ISC)

Copyright © 2016 Csaba Tuncsik <csaba.tuncsik@gmail.com>

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
